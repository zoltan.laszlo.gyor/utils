package utils;
import java.lang.StringBuilder;

public class IntList{
  private int[] array;

  public IntList(){

  }

  public IntList(int[] array){
    this.array = array;
  }

  public void add(int x){ // ahol nincs return->void
    int[] tmpArray = new int[array.length+1];
    for (int i = 0;i<array.length;i++ ) {
      tmpArray[i] = array[i];
    }
    tmpArray[array.length] = x;
    array = tmpArray;
  }

  public void add (int index, int x){
    int[] tmpArray = new int[array.length+1];
    for(int i=0; i< index; i++){
      tmpArray[i] = array[i];
    }
    tmpArray[index] = x;
    for(int i=index; i<array.length; i++){
      tmpArray[i+1] = array[i];
    }
    array = tmpArray;
  }

  public int get(){
    return array[0];
  }

  public int get(int index){
    return array[index];
  }

  public void set(int index, int x){
    if(index < array.length){
      array[index] = x;
    }
  }

  public int size(){
    return array.length;
  }

  public void remove(){
    int[] tmpArray = new int[array.length-1];
    for (int i = 1;i<array.length; i++ ) {
      tmpArray[i-1] = array[i];
    }
    array=tmpArray;
  }

  public void remove(int index){
    int[] tmpArray = new int[array.length-1];
    for(int i=0; i< index; i++){
      tmpArray[i] = array[i];
    }
    for(int i=index; i<array.length; i++){
      tmpArray[i-1] = array[i];
    }
    array = tmpArray;
  }

  public int indexOf(int x){
    for(int i = 0; i< array.length; i++){
      if(array[i] == 6 ){
        return i;
      }
    }
    return -1;
  }

  public void clear(){
      int[] tmpArray = new int[0];
      array = tmpArray;
  }


  public String toString(){ //nagym�ret� sz�vegn�l ez t kell haszn�lni
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i<array.length ;i++) {
      sb.append(array[i]); //=hozz�f�z�d
      sb.append(" ");
    }
    String finalText = sb.toString();
    return finalText;
  }
}
