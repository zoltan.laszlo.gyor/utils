import utils.IntList;

public class Main{
  public static void main (String args[]){
    int[] array = new int[10];

    for (int i =0; i<array.length; i++){
      array[i] = i;
      System.out.println(array[i]);
    }

    IntList list = new IntList(array);
    list.add(20);
    System.out.println(list.get());
    for( int i = 0 ; i < list.size(); i++){
      System.out.println(list.get(i));
    }
    list.set(3,5);
    System.out.println(list.get(3));
    list.remove();
    list.remove(3);
    System.out.println(list.indexOf(6));

    System.out.println(list.toString());
    list.clear();
  }
}
